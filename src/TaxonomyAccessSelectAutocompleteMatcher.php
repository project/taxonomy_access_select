<?php

namespace Drupal\taxonomy_access_select;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\taxonomy_access_select\TaxonomyAccessSelectPermissions;

class TaxonomyAccessSelectAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * Gets matched labels based on a given search string.
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    $matches = [];
    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    if ($target_type == 'taxonomy_term') {
      foreach ($options['target_bundles'] as $bundle) {
        $has_access = TaxonomyAccessSelectPermissions::selectAccess($bundle);
        if (!$has_access) {
          unset($options['target_bundles'][$bundle]);
        }
      }
    }
    $handler = $this->selectionManager
      ->getInstance($options);
    if (isset($string)) {

      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $entity_labels = $handler
        ->getReferenceableEntities($string, $match_operator, $match_limit);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $key = "{$label} ({$entity_id})";

          // Strip things like starting/trailing white spaces, line breaks and
          // tags.
          $key = preg_replace('/\\s\\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));

          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);
          $matches[] = [
            'value' => $key,
            'label' => $label,
          ];
        }
      }
    }
    return $matches;
  }

}
