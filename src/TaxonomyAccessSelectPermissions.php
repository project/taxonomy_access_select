<?php

namespace Drupal\taxonomy_access_select;

use Drupal\Core\Access\AccessResult;
use Drupal\taxonomy\Entity\Vocabulary;

class TaxonomyAccessSelectPermissions {

  /**
   * Permission callback.
   *
   * @see taxonomy_access_select.permissions.yml
   */
  public static function getAccess() {
    $vocabularies = Vocabulary::loadMultiple();

    $permissions = [];
    foreach ($vocabularies as $vocabulary) {
      $permissions['select terms in ' . $vocabulary->id()] = [
        'title' => t('Select terms in %vocabulary', ['%vocabulary' => $vocabulary->label()]),
      ];
    }

    return $permissions;
  }

  /**
   * Access callback for common CUSTOM taxonomy operations.
   */
  public static function selectAccess($vocabulary = NULL) {
    // Admin: always.
    if (\Drupal::currentUser()->hasPermission('administer taxonomy')) {
      return TRUE;
    }
    if ($vocabulary && is_string($vocabulary)) {
      $vocabulary = Vocabulary::load($vocabulary);
      if (\Drupal::currentUser()
        ->hasPermission('select terms in ' . $vocabulary->id())) {
        return TRUE;
      }
    }
    return FALSE;
  }
}
